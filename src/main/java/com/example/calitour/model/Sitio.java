package com.example.calitour.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "sitios", schema = "public")
public class Sitio {
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "nombresitio", nullable = false)
    private String nombresitio;

    @Column(name = "capacidad", nullable = false)
    private int capacidad;

    @Column(name = "descripcion", nullable = false)
    private String descripcion;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombresitio() {
        return nombresitio;
    }

    public void setNombresitio(String nombresitio) {
        this.nombresitio = nombresitio;
    }

    public int getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(int capacidad) {
        this.capacidad = capacidad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "Sitio{" +
                "id=" + id +
                ", nombresitio='" + nombresitio + '\'' +
                ", capacidad='" + capacidad + '\'' +
                ", descripcion='" + descripcion + '\'' +
                '}';
    }
}
