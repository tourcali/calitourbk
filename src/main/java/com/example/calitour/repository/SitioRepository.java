package com.example.calitour.repository;

import com.example.calitour.model.Sitio;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SitioRepository extends CrudRepository<Sitio, Long> {

}
