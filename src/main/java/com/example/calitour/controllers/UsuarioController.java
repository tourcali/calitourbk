package com.example.calitour.controllers;

import com.example.calitour.DTO.GenericDto;
import com.example.calitour.DTO.UsuarioDto;
import com.example.calitour.model.Usuario;
import com.example.calitour.security.JwtToken;
import com.example.calitour.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;

@RestController
@RequestMapping("/")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @PostMapping("/logear")
    @CrossOrigin(origins = "*")
    public ResponseEntity<GenericDto> logear(@RequestBody UsuarioDto usuarioDtox) throws NoSuchAlgorithmException {
        UsuarioDto usuarioDto = new UsuarioDto();
        System.out.println(usuarioDtox.toString());
        usuarioDto.setUser(usuarioDtox.getUser());
        usuarioDto.setPassword(usuarioDtox.getPassword());
        JwtToken objToken = new JwtToken();
        String token = objToken.getJWTToken(usuarioDtox.getUser());
        if (this.usuarioService.loguear(usuarioDto) != null) {
            return ResponseEntity.ok().body(GenericDto.sucess((token)));
        } else {
            return ResponseEntity.badRequest().body(GenericDto.error(("error")));
        }

    }

    @PostMapping("/registrar")
    @CrossOrigin(origins = "*")
    public ResponseEntity<GenericDto> registrar(@RequestBody UsuarioDto usuarioDto) {
        System.out.println(usuarioDto.toString());
        return ResponseEntity.ok().body(GenericDto.sucess(this.usuarioService.registrar(usuarioDto)));
    }

    @PutMapping("/actualizar")
    @CrossOrigin(origins = "*")
    public ResponseEntity<GenericDto> actualizar(@RequestBody UsuarioDto usuarioDto) {
        return ResponseEntity.ok().body(GenericDto.sucess(this.usuarioService.actualizar(usuarioDto)));
    }

    @DeleteMapping("/eliminar")
    @CrossOrigin(origins = "*")
    public ResponseEntity<GenericDto> eliminar(@RequestParam("id") Long id) {
        System.out.println(id);
        return ResponseEntity.ok().body(GenericDto.sucess(this.usuarioService.eliminar(id)));
    }

    @CrossOrigin(origins = "*")
    @PostMapping(path = "/cambiarclave")
    public ResponseEntity<GenericDto> cambiarclave(@RequestBody UsuarioDto usuarioDto) throws NoSuchAlgorithmException {
        Usuario usuarioCambiarClave = this.usuarioService.cambiarClave(usuarioDto);
        System.out.println(usuarioDto.toString());
        if (usuarioCambiarClave != null) {
            return ResponseEntity.ok().body(GenericDto.sucess(usuarioCambiarClave));
        } else {
            return ResponseEntity.badRequest().body(GenericDto.error("Los datos no coicide con el usuario ingresado"));
        }
    }

    @GetMapping("/consultarUsuarioById")
    public ResponseEntity<GenericDto> consultarUsuarioById(@RequestParam("id") Long id) {
        return ResponseEntity.ok().body(GenericDto.sucess(this.usuarioService.consultarUsuarioById(id)));
    }

    @GetMapping("/consultarUsuarios")
    public ResponseEntity<GenericDto> consultarUsuarios() {
        return ResponseEntity.ok().body(GenericDto.sucess(this.usuarioService.consultarUsuarios()));
    }
}
