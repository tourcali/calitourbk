package com.example.calitour.controllers;

import com.example.calitour.DTO.GenericDto;
import com.example.calitour.DTO.SitioDto;
import com.example.calitour.service.SitioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/sitio")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class SitioController {

    @Autowired
    private SitioService sitioService;

    @PostMapping("/registrarSitio")
    @CrossOrigin(origins = "*")
    public ResponseEntity<GenericDto> registrarSitio(@RequestBody SitioDto sitioDto) {
        System.out.println(sitioDto.toString());
        return ResponseEntity.ok().body(GenericDto.sucess(this.sitioService.registrarSitio(sitioDto)));
    }

    @GetMapping("/consultarSitioById")
    public ResponseEntity<GenericDto> consultarSitioById(@RequestParam("id") Long id) {
        return ResponseEntity.ok().body(GenericDto.sucess(this.sitioService.consultarSitioById(id)));
    }

    @GetMapping("/consultarSitio")
    public ResponseEntity<GenericDto> consultarSitio() {
        return ResponseEntity.ok().body(GenericDto.sucess(this.sitioService.consultarSitio()));
    }

    @DeleteMapping("/eliminarSitio")
    @CrossOrigin(origins = "*")
    public ResponseEntity<GenericDto> eliminarSitio(@RequestParam("id") Long id) {
        System.out.println(id);
        return ResponseEntity.ok().body(GenericDto.sucess(this.sitioService.eliminarSitio(id)));
    }

}
