package com.example.calitour.DTO;

public class UsuarioDto {
    private Long id;
    private String nombre;
    private String apellido;
    private String correo;
    private String user;
    private String password;
    private String rol;

    public Long getId() {
        return this.id;
    }

    public void setId(Long idSet) {
        this.id = idSet;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombreSet) {
        this.nombre = nombreSet;
    }

    public String getApellido() {
        return this.apellido;
    }

    public void setApellido(String apellidoSet) {
        this.apellido = apellidoSet;
    }

    public String getCorreo() {
        return this.correo;
    }

    public void setCorreo(String correoSet) {
        this.correo = correoSet;
    }

    public String getUser() {
        return this.user;
    }

    public void setUser(String userSet) {
        this.user = userSet;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String passwordSet) {
        this.password = passwordSet;
    }

    public String getRol() {
        return this.rol;
    }

    public void setRol(String rolSet) {
        this.rol = rolSet;
    }

    @Override
    public String toString() {
        return "UserDto{" +
                "id" + id +
                "nombre" + nombre + '\'' +
                "apellido" + apellido + '\'' +
                "correo" + correo + '\'' +
                "user" + user + '\'' +
                "password" + password + '\'' +
                "rol" + rol + '\'' +
                "}";

    }
}
