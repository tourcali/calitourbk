package com.example.calitour.DTO;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class GenericDto {
    private Integer status;
    private Object payload;

    public static GenericDto sucess(Object data) {

        GenericDto genericDto = new GenericDto();
        genericDto.status = (HttpStatus.OK.value());
        genericDto.payload = (data);

        return genericDto;
    }
    public static GenericDto error(Object data) {

        GenericDto genericDto = new GenericDto();
        genericDto.status = (HttpStatus.BAD_REQUEST.value());
        genericDto.payload = (data);

        return genericDto;
    }

}
