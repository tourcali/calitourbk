package com.example.calitour.service;

import com.example.calitour.DTO.UsuarioDto;
import com.example.calitour.model.Usuario;

import java.security.NoSuchAlgorithmException;
import java.util.List;

public interface UsuarioService {
    Usuario loguear(UsuarioDto usuarioDto) throws NoSuchAlgorithmException;

    Usuario cambiarClave(UsuarioDto usuarioDto) throws NoSuchAlgorithmException;

    String registrar(UsuarioDto usuarioDto);

    String actualizar(UsuarioDto usuarioDto);

    String eliminar(Long id);

    Usuario consultarUsuarioById(Long id);

    List<Usuario> consultarUsuarios();

}
