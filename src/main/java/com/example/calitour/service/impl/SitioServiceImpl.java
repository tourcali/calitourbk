package com.example.calitour.service.impl;

import com.example.calitour.DTO.SitioDto;
import com.example.calitour.model.Sitio;
import com.example.calitour.repository.SitioRepository;
import com.example.calitour.service.SitioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SitioServiceImpl implements SitioService {
    @Autowired
    private SitioRepository sitioRepository;

    @Override
    public String registrarSitio(SitioDto sitioDtoSave) {
        Sitio dtoSitioSave = new Sitio();
        dtoSitioSave.setId(sitioDtoSave.getId());
        dtoSitioSave.setNombresitio(sitioDtoSave.getNombresitio());
        dtoSitioSave.setCapacidad(sitioDtoSave.getCapacidad());
        dtoSitioSave.setDescripcion(sitioDtoSave.getDescripcion());
        sitioRepository.save(dtoSitioSave);
        return "Registro Exitoso";
    }

    @Override
    public Sitio consultarSitioById(Long id) {
        return sitioRepository.findById(id).orElse(null);
    }

    @Override
    public List<Sitio> consultarSitio() {
        return (List<Sitio>) sitioRepository.findAll();
    }

    @Override
    public String eliminarSitio(Long id) {
        sitioRepository.deleteById(id);
        return "Eliminado el Sitio con Exito";
    }
}
