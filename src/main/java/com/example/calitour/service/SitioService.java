package com.example.calitour.service;

import com.example.calitour.DTO.SitioDto;
import com.example.calitour.model.Sitio;

import java.util.List;


public interface SitioService {

    String registrarSitio(SitioDto sitioDto);

    Sitio consultarSitioById(Long id);

    List<Sitio> consultarSitio();

    String eliminarSitio (Long id);

}
